﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memory_Game
{
    public partial class Form3 : Form
    {
        int attempts, time;

        private void Form3_Load(object sender, EventArgs e)
        {
            label3.Text += attempts.ToString();
            label4.Text += ((time >= 60) ? (time / 60).ToString() + "m " : "") + (time % 60).ToString() + "s";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() != "")
            {
                this.Hide();
                Form2 form2 = new Form2(textBox1.Text, attempts, time);
                form2.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Insert a username!");
            }
        }

        public Form3(int attmp, int t)
        {
            InitializeComponent();
            attempts = attmp;
            time = t;
        }
    }
}
