﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memory_Game
{
    public partial class Form2: Form
    {
        string name;
        int attempts, time;
        List<Player> players;  //Λίστα απο παίχτες
        BinaryFormatter bf = new BinaryFormatter(); //Για το Serialization

        public Form2()
        {
            InitializeComponent();
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            richTextBox1.Location = new Point(15, 15);
            richTextBox1.Height = button1.Location.Y - 2 * richTextBox1.Location.Y;
            richTextBox1.Width = this.Width - 3 * richTextBox1.Location.X;
            button1.Location = new Point((richTextBox1.Width / 2) + richTextBox1.Location.X - (button1.Width / 2) ,button1.Location.Y);
        }
        public Form2(string n, int attmps,int t)
        {
            InitializeComponent();
            name = n;
            attempts = attmps;
            time = t;
            label3.Text += name;
            label1.Text += attempts.ToString();
            label2.Text += (time >= 60) ? (time / 60).ToString() + "m " : "";
            label2.Text += (time % 60).ToString() + "s";
        }

        private void FillList()
        {
            players = Deserialize();
            if (name != null)
            {
                Player thisplayer = players.Find(x => x.Username == name);
                if (thisplayer != null)
                {
                    thisplayer.Attempts.Add(attempts);
                    label4.Text += thisplayer.Attempts.Sum() / thisplayer.Attempts.Count;
                }
                else
                {
                    Player t = new Player(name);
                    t.Attempts.Add(attempts);
                    players.Add(t);
                }
                Serialize(players);
            }
        }
        
        private List<Player> Deserialize()
        {
            List<Player> players = new List<Player>();
            Stream st = new FileStream("Players.txt", FileMode.Open, FileAccess.Read);
            try
            {
                players = (List<Player>)bf.Deserialize(st);
            }
            catch (Exception){}
            finally { st.Close(); }
            return players;
        }

        private void Serialize(List<Player> players)
        {
            Stream st = new FileStream("Players.txt", FileMode.Create, FileAccess.Write);
            try
            {
                bf.Serialize(st, players);
            }
            catch(Exception)
            {
                st.Close();
            }
            st.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            FillList();
            players= players.OrderBy(i => i.MinAttempts()).ToList();
            foreach (Player i in players)
            {
                richTextBox1.AppendText(i.Username + " " + i.MinAttempts().ToString() + Environment.NewLine);
                if (richTextBox1.Lines.Length == 11) break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
