﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memory_Game
{
    class Card
    {
        private PictureBox pictureBox;
        private Image image;

        public PictureBox PictureBox { get => pictureBox; set => pictureBox = value; }
        public Image Image { get => image; set => image = value; }

        public Card()
        {
        }

        public Card(PictureBox pictureBox, Image image) {
            this.pictureBox = pictureBox;
            this.image = image;
        }
    }
}
