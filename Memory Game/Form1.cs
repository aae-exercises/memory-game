﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memory_Game
{
    public partial class Form1 : Form
    {
        string path = Application.StartupPath + "\\Pictures";
        List<Card> cards;   //Για την αντιστοιχία κάθε picturebox σε μια εικόνα
        PictureBox firstFlip, secondFlip;
        int pairsFound, attempts, time;

        public Form1()
        {
            InitializeComponent();
            if (!File.Exists("Players.txt"))
            {
                File.Create("Players.txt");
            }
        }

        private void NewGame() {
            firstFlip = null;
            //Για κάθε PictureBox
            foreach (PictureBox p in this.Controls.OfType<PictureBox>())
            {
                //Επαναφέρει όλα τα PictureBox στην αρχική κατάσταση
                p.Image = Properties.Resources.card_back;
                p.Enabled = true;
                p.Visible = true;
            }
            pairsFound = 0;
            attempts = 0;
            time = 0;
            cards.Clear();
            LoadPictures(path);
            timer1.Start();
        }

        private bool LoadPictures(string path) {
            //Κάθε φορά που ξεκινάει νέο παιχνίδι
            Random rnd = new Random();
            List<Image> temp = new List<Image>();

            foreach (string fileName in Directory.GetFiles(path, "*.png"))      //Κάθε png αρχείο
                if (temp.Count < 20)                                           //Αν δεν έχουν προστεθεί 20 αρχεία
                {                                                             //Προστίθεται 2 φορές
                    Image image = Image.FromFile(fileName); //Για να προσθέσουμε 2 φορές το ίδιο αντικείμενο
                    for (int i = 0; i < 2; i++)
                        temp.Add(image);
                }
            foreach (string fileName in Directory.GetFiles(path, "*.jpg"))  //Η ίδια διαδικασία για jpg
                if (temp.Count < 20)
                {
                    Image image = Image.FromFile(fileName);
                    for (int i = 0; i < 2; i++)
                        temp.Add(image);
                }

            if (temp.Count == 20)                                   //Αν υπάρχουν 10 εικόνες (απο 2 φορές η κάθε μία)
            {
                temp = temp.OrderBy(x => rnd.Next()).ToList();     //Οι εικόνες ανακατέυονται
                int index = 0;
                foreach (PictureBox c in this.Controls.OfType<PictureBox>())
                    cards.Add(new Card((PictureBox)c, temp[index++]));    //Προσθέτει κάθε PictureBox με την αντίστοιχη εικόνα στο pictures (Dictionary)
                return true;                                          //Και επιστρέφει true
            }
            else
            {
                MessageBox.Show("Not enough pictures found in " + path, "More pictures needed!");
                return false;                                   //Αλλιώς επιστρέφει false
            }
        }

        private async void Flip(PictureBox p)
        {
            attempts++;
            if (firstFlip == null)                    //Επιλογή πρώτης εικόνας
            {
                firstFlip = p;
                firstFlip.Image = GetCardImage(firstFlip);
                firstFlip.Enabled = false;          //Απενεργοποίηση πρώτης εικόνας
            }
            else if (secondFlip == null)              //Επιλογή δεύτερης εικόνας
            {
                secondFlip = p;
                secondFlip.Image = GetCardImage(secondFlip);
                secondFlip.Enabled = false;          //Απενεργοποίηση δεύτερης εικόνας                 
                await Task.Delay(500);              //Περιμένει 500 ms
                if (firstFlip.Image == secondFlip.Image)    //Αν είναι ίδιες
                {
                    firstFlip.Visible = false;          //Eξαφανίζονται
                    secondFlip.Visible = false;
                    if (++pairsFound == 10)
                    {
                        timer1.Stop();
                        Form3 form3 = new Form3(attempts,time);
                        form3.ShowDialog();
                        NewGame();
                    }
                }
                else {
                    firstFlip.Enabled = true;   //Αν οι εικόνες δεν είναι ίδιες ξαναενεργοποιούνται
                    secondFlip.Enabled = true;
                    firstFlip.Image = Properties.Resources.card_back;   //Οι εικόνες ξαναγυρίζουν
                    secondFlip.Image = Properties.Resources.card_back;
                }
                    
                firstFlip = null;
                secondFlip = null;
            }
        }

        private void SelectDirectory()
        {
            //Επιλογή φακέλου 
            FolderBrowserDialog browser_dialog = new FolderBrowserDialog();
            //Αν ο χρήστης πατήσει ΟΚ
            if (browser_dialog.ShowDialog() == DialogResult.OK)
            {
                path = browser_dialog.SelectedPath;
            }
        }

        private Image GetCardImage(PictureBox pb) {
            foreach (Card card in cards)
                if (card.PictureBox == pb)
                    return card.Image;
            return new Card().Image;
        }

        private void changeImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectDirectory();
            NewGame();
        }

        private void loadDefautPicturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            path = Application.StartupPath + "\\Pictures";
            NewGame();
        }

        private void bestPlayersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
            NewGame();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (Control p in this.Controls.OfType<PictureBox>())
                p.Click += (object Sender, EventArgs E)=> Flip((PictureBox)Sender);
            cards = new List<Card>();
            NewGame();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time++;
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            NewGame();
        }
    }
}
