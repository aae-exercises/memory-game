﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memory_Game
{
    [Serializable]
    class Player
    {
        private string username;
        private List<int> attempts;
        public string Username { get => username; set => username = value; }
        public List<int> Attempts { get => attempts; set => attempts = value; }
        //Θα γίνεται προσθήκη προσπαθειών ως εξής: player.Attempts.Add(a); (έστω ότι player είναι το object και a οι προσπάθειες που χρειάστηκε)

        public Player(string username) {
            this.username = username;
            this.attempts = new List<int>();
        }
        public int MinAttempts()
        {
            attempts.Sort();
            return attempts[0];
        }
    }
}
